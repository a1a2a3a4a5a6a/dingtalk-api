/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 * 整体跳转ActionCard样式
 * https://ding-doc.dingtalk.com/doc#/serverapi2/ye8tup
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:55
 */
public class DingMessageCardWhole extends DingMessage{

    private final String title;
    private final String markdown;
    private final String singleTitle;
    private final String singleUrl;

    public DingMessageCardWhole(String title, String markdown, String singleTitle, String singleUrl) {
        super(MESSAGE_CARD);
        this.title = title;
        this.markdown = markdown;
        this.singleTitle = singleTitle;
        this.singleUrl = singleUrl;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> card=new HashMap<>();
        card.put("title",title);
        card.put("markdown",markdown);
        card.put("single_url",singleUrl);
        card.put("single_title",singleTitle);

        map.put("action_card",card);

        return JSON.toJSONString(map);
    }
}
