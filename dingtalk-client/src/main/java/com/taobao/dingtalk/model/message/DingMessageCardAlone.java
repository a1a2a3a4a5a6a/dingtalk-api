/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 独立跳转ActionCard样式
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 11:00
 */
public class DingMessageCardAlone extends DingMessage{

    private final String title;
    private final String markdown;
    private final String orientation;
    private final List<CardInfo> cardInfos;

    public DingMessageCardAlone(String title, String markdown, String orientation, List<CardInfo> cardInfos) {
        super(MESSAGE_CARD);
        this.title = title;
        this.markdown = markdown;
        this.orientation = orientation;
        this.cardInfos = cardInfos;
    }

    public class CardInfo{
        private String title;

        @JSONField(name = "action_url")
        private String actionUrl;

        public CardInfo(String title, String actionUrl) {
            this.title = title;
            this.actionUrl = actionUrl;
        }
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> card=new HashMap<>();
        card.put("title",title);
        card.put("markdown",markdown);
        card.put("btn_orientation",orientation);
        card.put("btn_json_list",cardInfos);

        map.put("action_card",card);
        return JSON.toJSONString(map);
    }
}
