package com.dingtalk.api.request;

import java.util.List;
import com.taobao.api.internal.mapping.ApiField;
import com.taobao.api.internal.mapping.ApiListField;
import com.taobao.api.TaobaoObject;
import java.util.Map;
import java.util.List;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.dingtalk.api.DingTalkConstants;
import com.taobao.api.Constants;
import com.taobao.api.internal.util.TaobaoHashMap;
import com.taobao.api.internal.util.TaobaoUtils;
import com.taobao.api.internal.util.json.JSONWriter;
import com.dingtalk.api.response.OapiWorkspaceProjectMemberRoleUpdateResponse;

/**
 * TOP DingTalk-API: dingtalk.oapi.workspace.project.member.role.update request
 * 
 * @author top auto create
 * @since 1.0, 2019.09.16
 */
public class OapiWorkspaceProjectMemberRoleUpdateRequest extends BaseTaobaoRequest<OapiWorkspaceProjectMemberRoleUpdateResponse> {
	
	

	/** 
	* 成员设置角色
	 */
	private String role;

	public void setRole(String role) {
		this.role = role;
	}

	public void setRole(OpenMemberRoleAddDto role) {
		this.role = new JSONWriter(false,false,true).write(role);
	}

	public String getRole() {
		return this.role;
	}

	public String getApiMethodName() {
		return "dingtalk.oapi.workspace.project.member.role.update";
	}

	private String topResponseType = Constants.RESPONSE_TYPE_DINGTALK_OAPI;

     public String getTopResponseType() {
        return this.topResponseType;
     }

     public void setTopResponseType(String topResponseType) {
        this.topResponseType = topResponseType;
     }

     public String getTopApiCallType() {
        return DingTalkConstants.CALL_TYPE_OAPI;
     }

     private String topHttpMethod = DingTalkConstants.HTTP_METHOD_POST;

     public String getTopHttpMethod() {
     	return this.topHttpMethod;
     }

     public void setTopHttpMethod(String topHttpMethod) {
        this.topHttpMethod = topHttpMethod;
     }

     public void setHttpMethod(String httpMethod) {
         this.setTopHttpMethod(httpMethod);
     }

	public Map<String, String> getTextParams() {		
		TaobaoHashMap txtParams = new TaobaoHashMap();
		txtParams.put("role", this.role);
		if(this.udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public Class<OapiWorkspaceProjectMemberRoleUpdateResponse> getResponseClass() {
		return OapiWorkspaceProjectMemberRoleUpdateResponse.class;
	}

	public void check() throws ApiRuleException {
	}
	
	/**
	 * 角色，不允许空，list内的元素不允许null。最多20个
	 *
	 * @author top auto create
	 * @since 1.0, null
	 */
	public static class OpenTagDto extends TaobaoObject {
		private static final long serialVersionUID = 8356582898615724173L;
		/**
		 * 角色code
		 */
		@ApiField("code")
		private String code;
		/**
		 * 角色名
		 */
		@ApiField("name")
		private String name;
	
		public String getCode() {
			return this.code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getName() {
			return this.name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	/**
	 * 成员设置角色
	 *
	 * @author top auto create
	 * @since 1.0, null
	 */
	public static class OpenMemberRoleAddDto extends TaobaoObject {
		private static final long serialVersionUID = 4782215882354988153L;
		/**
		 * 角色，不允许空，list内的元素不允许null。最多20个
		 */
		@ApiListField("tags")
		@ApiField("open_tag_dto")
		private List<OpenTagDto> tags;
		/**
		 * 成员id
		 */
		@ApiField("userid")
		private String userid;
	
		public List<OpenTagDto> getTags() {
			return this.tags;
		}
		public void setTags(List<OpenTagDto> tags) {
			this.tags = tags;
		}
		public String getUserid() {
			return this.userid;
		}
		public void setUserid(String userid) {
			this.userid = userid;
		}
	}
	

}